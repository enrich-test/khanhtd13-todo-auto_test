
describe('Demo test Todo', () => {

    it("open web", () => {
        cy.visit("http://localhost:3000/");
    });

    it('create new task', () => {
        cy.get('.add-new-task-border input').type('Ruby');
        cy.get('.add-new-task-border button').click();
        cy.get('.task-list-content .task-item').should(($elem) => {
            expect($elem).to.have.length(1),
            expect($elem.eq(0), 'first item').to.contain('Ruby')
        })
    });

    it('completed task', () => {
        cy.get('.task-item').click();
        cy.get('.task-list-content .task-item_completed').should(($elem) => {
            expect($elem).to.have.length(1),
            expect($elem.eq(0), 'first item').to.contain('Ruby')
        })
    });

    it('imcompleted task', () => {
        cy.get('.task-item_completed').click();
        cy.get('.task-list-content .task-item').should(($elem) => {
            expect($elem).to.have.length(1),
            expect($elem.eq(0), 'first item').to.contain('Ruby')
        })
    });
    
    it('create the second task', () => {
        cy.get('.add-new-task-border input').clear() 
        cy.get('.add-new-task-border input').type('Java');
        cy.get('.add-new-task-border button').click();
        cy.get('.task-list-content .task-item').should(($elem) => {
            expect($elem).to.have.length(2),
            expect($elem.eq(1), 'second item').to.contain('Java')
        })
    });
});
