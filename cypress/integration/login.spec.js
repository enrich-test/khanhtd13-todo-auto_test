
describe('Demo test Login', () => {

    it("open web", () => {
        cy.visit("http://localhost:3000/");
    });
    it('should redirect to login page', () => {
        cy.get('a').click();
        cy.get('#root  h1').should(($elem) => {
            expect($elem).to.have.length(1),
            expect($elem).to.contain('Login')
        })
    });
   
});
